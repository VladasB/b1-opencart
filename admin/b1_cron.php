<?php

// Version
define('VERSION', '2.3.0.2');

// Configuration
if (is_file('config.php')) {
    require_once('config.php');
}

// Install
if (!defined('DIR_APPLICATION')) {
    header('Location: ../install/index.php');
    exit;
}
require_once(DIR_SYSTEM . 'library/vendor/b1/libb1/B1.php');
require_once(DIR_SYSTEM . 'library/request.php');
require_once(DIR_SYSTEM . 'library/db.php');
require_once(DIR_SYSTEM . 'library/db/' . DB_DRIVER . '.php');
require_once(DIR_SYSTEM . 'library/config.php');
require_once(DIR_SYSTEM . 'engine/event.php');
require_once(DIR_SYSTEM . 'engine/loader.php');
require_once(DIR_SYSTEM . 'engine/model.php');
require_once(DIR_SYSTEM . 'engine/registry.php');
if (is_file(DIR_SYSTEM . 'engine/proxy.php')) {
    require_once(DIR_SYSTEM . 'engine/proxy.php');
}

class B1CronException extends Exception
{

    private $extraData;

    public function __construct($message = "", $extraData = array(), $code = 0, \Exception $previous = null)
    {
        $this->extraData = $extraData;
        parent::__construct($message, $code, $previous);
    }

    public function getExtraData()
    {
        return $this->extraData;
    }

}

class B1Cron
{

    const TTL = 3600;
    const MAX_ITERATIONS = 100;
    const ORDERS_PER_ITERATION = 100;
    const ORDER_SYNC_THRESHOLD = 10;
    const PLUGIN_NAME = 'Opencart';

    /**
     * @var Request
     */
    private $request;

    /**
     * @var DB
     */
    private $db;

    /**
     * @var ModelB1Settings
     */
    private $settings;

    /**
     * @var ModelB1Helper
     */
    private $helper;

    /**
     * @var ModelB1Items
     */
    private $items;

    /**
     * @var ModelB1Orders
     */

    private $orders;
    /**
     * @var B1
     */
    private $library;

    private function init()
    {
        set_time_limit(self::TTL);
        ini_set('max_execution_time', self::TTL);

        // Registry
        $registry = new Registry();
        // Config
        $config = new Config();
        $registry->set('config', $config);
        // Event
        $event = new Event($registry);
        $registry->set('event', $event);
        // Loader
        $loader = new Loader($registry);
        $registry->set('load', $loader);
        // Request
        $registry->set('request', new Request());
        // Database
        $registry->set('db', new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE));

        $this->request = $registry->get('request');
        $this->db = $registry->get('db');

        $loader->model('b1/settings');
        $this->settings = $registry->get('model_b1_settings');
        $loader->model('b1/helper');
        $this->helper = $registry->get('model_b1_helper');
        $loader->model('b1/items');
        $this->items = $registry->get('model_b1_items');
        $loader->model('b1/orders');
        $this->orders = $registry->get('model_b1_orders');

    }

    private function validateAccess()
    {
        $cron_key = $this->settings->get('cron_key');
        if ($cron_key == null) {
            throw new B1CronException('Fatal error.');
        }
        if (isset($this->request->get['key'])) {
            $get_key = $this->request->get['key'];
        } else {
            $get_key = null;
        }
        if ($get_key != $cron_key) {
            exit();
        }
    }

    public function __construct()
    {
        $this->init();
        $this->validateAccess();

        if (isset($this->request->get['id'])) {
            $get_id = $this->request->get['id'];
        } else {
            $get_id = null;
        }
        try {
            $this->library = new B1([
                'apiKey' => $this->settings->get('api_key'),
                'privateKey' => $this->settings->get('private_key')
            ]);
        } catch (B1Exception $e) {
            throw new B1CronException('API/Private key is not provided');
        }

        switch ($get_id) {
            case 'products':
                $this->fetchProducts(true);
                break;
            case 'updatedProducts':
                $this->fetchProducts(false);
                break;
            case 'orders':
                $this->syncOrders();
                break;
            default:
                throw new B1CronException('Bad action ID specified.');
        }
    }

    private function fetchProducts($syncAll = true)
    {
        $i = 0;
        $lid = null;
        $modAfterDate = $syncAll ? null : $this->settings->get('latest_product_sync_date');
        $enableQuantitySync = $this->settings->get('quantity_sync');
        if (!$enableQuantitySync) {
            throw new B1CronException('Not set quantity_sync value');
        }
        if ($syncAll) {
            $this->items->resetAllB1ReferenceId();
        }
        do {
            $i++;
            try {
                $response = $this->library->request('shop/product/list', ["lid" => $lid, "lmod" => $modAfterDate]);
                $data = $response->getContent();
                foreach ($data['data'] as $item) {
                    if ($item['code'] != NULL) {
                        $this->items->updateProductReferenceId($item['id'], $item['code']);
                        if ($enableQuantitySync == 1) {
                            $this->items->updateProductQuantity($item['quantity'], $item['id']);
                        }
                    }
                }
                if (count($data['data']) == 100) {
                    $lid = $data['data'][99]['id'];
                } else {
                    $lid = -1;
                }

            } catch (B1Exception $e) {

                $this->helper->printPre($e->getMessage());
                $this->helper->printPre($e->getExtraData());
            }
            echo "$i;";
        } while ($lid != -1 && $i < self::MAX_ITERATIONS);
        if ($syncAll) {
            $this->settings->set('initial_product_sync', 1);
        } else {
            $this->settings->set('latest_product_sync_date', date("Y-m-d"));
        }
        echo 'OK';
    }

    private function syncOrders()
    {
        try {
            $orders_sync_from = $this->settings->get('orders_sync_from');
            if ($orders_sync_from == null) {
                throw new B1CronException('Not set orders_sync_from value');
            }
            $order_status_id = $this->settings->get('order_status_id');
            if ($order_status_id == null) {
                throw new B1CronException('Not set orders_status_id value');
            }
            $data_prefix = $this->settings->get('shop_id');
            if ($data_prefix == null) {
                throw new B1CronException('Not set shop_id value');
            }
            $initial_sync = $this->settings->get('initial_sync');
            if ($initial_sync == null) {
                throw new B1CronException('Not set initial_sync value');
            }
            $enableQuantitySync = $this->settings->get('quantity_sync');
            if (!$enableQuantitySync) {
                throw new B1CronException('Not set quantity_sync value');
            }
            $initial_product_sync = $this->settings->get('initial_product_sync');
            if (!$initial_product_sync) {
                throw new B1CronException('Initial product sync is not done yet.');
            }
            $i = 0;
            do {
                ob_start();
                $i++;
                $processed = 0;
                $orders = $this->orders->getOrdersForSync(self::ORDERS_PER_ITERATION, $order_status_id);
                foreach ($orders->rows as $item) {
                    $order_data = $this->generateOrderData($item, $order_status_id, $data_prefix, $initial_sync);
                    try {
                        $response = $this->library->request('shop/order/add', $order_data['data']);
                        $request = $response->getContent();
                        $this->orders->assignB1OrderId($request['data']['orderId'], $item['order_id']);
                        if ($enableQuantitySync == 1 && isset($request['data']['update'])) {
                            foreach ($request['data']['update'] as $productQuantity) {
                                $this->items->updateProductQuantity($productQuantity['quantity'], $request['data']['orderId']);
                            }
                        }
                    } catch (B1DuplicateException $e) {
                        // @todo exception errors
                        $content = $e->getResponse()->getContent();
                        $this->orders->assignB1OrderId($content['data']['orderId'], $item['order_id']);
                    }

                    $processed++;
                    echo "$i-$processed;";
                }
                ob_end_flush();
            } while ($processed == self::ORDERS_PER_ITERATION && $i < self::MAX_ITERATIONS);
            if ($initial_sync == 0) {
                $this->settings->set('initial_sync', 1);
            }

            echo 'OK';
        } catch (B1CronException $e) {
            throw new $e;
        }
    }

    private function generateOrderData($item, $order_status_id, $data_prefix, $initial_sync)
    {
        $order_tax = $this->db->query("SELECT `rate` FROM " . DB_PREFIX . "order_total LEFT JOIN " . DB_PREFIX . "tax_rate ON title = name  WHERE `code` = 'tax' AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        $shipping = $this->db->query("SELECT `value` FROM " . DB_PREFIX . "order_total WHERE `code` = 'shipping' AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        $discount = $this->db->query("SELECT `value` FROM " . DB_PREFIX . "order_total WHERE `code` = 'coupon' AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        $order_status = $this->db->query("SELECT `date_added` FROM " . DB_PREFIX . "order_history WHERE `order_status_id` = " . $this->db->escape($order_status_id) . " AND `order_id` = " . $this->db->escape($item['order_id']))->row;
        if (empty($order_status)) {
            throw new B1CronException('Not found order_history data, for ' . $this->db->escape($item['order_id']) . ' order.', array(
                'order_status_id' => $order_status_id,
                'order' => $item,
            ));
        }

        $order_data = array();
        $order_data['pluginName'] = self::PLUGIN_NAME;
        $order_data['prefix'] = $data_prefix;
        $order_data['orderId'] = $item['order_id'];
        $order_data['orderDate'] = substr($order_status['date_added'], 0, 10);
        $order_data['orderNo'] = $item['order_id'];
        $order_data['currency'] = $item['currency_code'];
        if (isset($shipping['value'])) {
            $order_data['shippingAmount'] = intval(round($shipping['value'] * $item['currency_value'] * 100));
        }
        if (isset($discount['value'])) {
            $order_data['discount'] = intval(round(abs($discount['value']) * $item['currency_value'] * 100));
        } else {
            $order_data['discount'] = 0;
        }
        $order_data['total'] = intval(round($item['total'] * $item['currency_value'] * 100));
        $order_data['orderEmail'] = $item['email'];

        if (!isset($order_tax['rate'])) {
            $order_data['vatRate'] = "0";
        } else {
            $order_data['vatRate'] = intval(round($order_tax['rate']));
        }
        $order_data['writeOff'] = $initial_sync;
        $order_data['billing']['name'] = empty($item['payment_company']) ? trim($item['payment_firstname'] . ' ' . $item['payment_lastname']) : $item['payment_company'];
        $order_data['billing']['isCompany'] = $item['payment_company'] == '' ? 0 : 1;
        $order_data['billing']['address'] = $item['payment_address_1'];
        $order_data['billing']['city'] = $item['payment_city'];
        $order_data['billing']['country'] = $item['iso_code_2'];

        $order_data['delivery']['name'] = empty($item['shipping_company']) ? trim($item['shipping_firstname'] . ' ' . $item['shipping_lastname']) : $item['shipping_company'];
        $order_data['delivery']['isCompany'] = $item['shipping_company'] == '' ? 0 : 1;
        $order_data['delivery']['address'] = $item['shipping_address_1'];
        $order_data['delivery']['city'] = $item['shipping_city'];
        $order_data['delivery']['country'] = $item['iso_code_2'];
        if ($order_data['billing']['name'] == '') {
            $order_data['billing'] = $order_data['delivery'];
        }
        if ($order_data['delivery']['name'] == '') {
            $order_data['delivery'] = $order_data['billing'];
        }
        $order_products = $this->db->query("SELECT op.*, p.b1_reference_id FROM " . DB_PREFIX . "order_product op LEFT OUTER JOIN " . DB_PREFIX . "product p ON p.product_id = op.product_id WHERE order_id = " . $this->db->escape($item['order_id']));
        if (empty($order_products->rows)) {
            throw new B1CronException('Not found order_products data, for ' . $this->db->escape($item['order_id']) . ' order.', array('order_data' => $order_data));
        }
        foreach ($order_products->rows as $key => $product) {
            $order_data['items'][$key]['id'] = $product['b1_reference_id'];
            $order_data['items'][$key]['name'] = $product['name'];
            $order_data['items'][$key]['quantity'] = intval(round($product['quantity'] * 100));
            $order_data['items'][$key]['price'] = intval(round($product['price'] * $item['currency_value'] * 100));
            $order_data['items'][$key]['sum'] = intval(round($product['price'] * $item['currency_value'] * $product['quantity'] * 100));
        }
        return [
            'data' => $order_data
        ];
    }

}

new B1Cron;
