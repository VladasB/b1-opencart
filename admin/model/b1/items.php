<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Items extends ModelB1Base
{

    public  function resetAllB1ReferenceId()
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET b1_reference_id = NULL ");

    }

    public function updateProductReferenceId($id, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET b1_reference_id = " . $id . " WHERE sku ='" . $code. "'");

    }

    public  function updateProductQuantity($quantity, $code)
    {
        $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . $quantity . "' WHERE b1_reference_id = " . $code);

    }

}
