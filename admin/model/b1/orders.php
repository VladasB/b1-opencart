<?php

require_once(DIR_APPLICATION . 'model/b1/base.php');

class ModelB1Orders extends ModelB1Base
{
    public function assignB1OrderId($b1_order, $order_id)
    {
        return $this->db->query("UPDATE " . DB_PREFIX . "order SET b1_reference_id = " . $b1_order . " WHERE order_id = " . $order_id);

    }

    public function getOrdersForSync($iterations, $order_status_id)
    {
        return $this->db->query("SELECT DISTINCT o.order_id as int_id, o.*, c.*, oh.* FROM " . DB_PREFIX . "order o
                    LEFT JOIN " . DB_PREFIX . "country c ON o.payment_country_id = c.country_id
                    LEFT JOIN " . DB_PREFIX . "order_history AS oh on o.order_id = oh.order_id
                    WHERE  b1_reference_id IS NULL AND oh.order_status_id = '" . $order_status_id . "' ORDER BY oh.date_added ASC LIMIT " . $iterations);

    }


    public function getOrderStatuses()
    {
        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "order_status` WHERE language_id = 1 ORDER BY order_status_id");
        return $query->rows;
    }


}
