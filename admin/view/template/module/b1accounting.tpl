<?php echo $header; ?>
<?php echo $column_left; ?>

<div id="content" class="b1-extension">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <a href="mailto:<?php echo $url_contact; ?>" data-toggle="tooltip"
                   title="<?php echo $button_contact; ?>" class="btn btn-default">
                    <i class="fa fa-envelope"></i>
                </a>
                <a href="<?php echo $url_documentation; ?>" data-toggle="tooltip"
                   title="<?php echo $button_documentation; ?>" target="_blank" class="btn btn-default">
                    <i class="fa fa-book"></i> <span class="hidden-xs"><?php echo $button_documentation; ?></span>
                </a>
                <a href="<?php echo $url_help_page; ?>" data-toggle="tooltip" title="<?php echo $button_help_page; ?>"
                   target="_blank" class="btn btn-default">
                    <i class="fa fa-book"></i> <span class="hidden-xs"><?php echo $button_help_page ?></span>
                </a>
                <a href="<?php echo $url_cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>"
                   class="btn btn-default">
                    <i class="fa fa-reply"></i>
                </a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <div class="tab-content">
            <div class="tab-pane active" id="settings">
                <div class="row">
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-cogs"></i> <?php echo $text_configuration; ?>
                                </h3>
                            </div>
                            <div class="panel-body">
                                <form action="<?php echo $form_action; ?>" method="post" class="form-horizontal">
                                    <input type="hidden" name="form" value="configuration">
                                    <div class="alert alert-success hidden" id="success-msg" role="alert">
                                        <button type="button" class="close"><span aria-hidden="true">&times;</span>
                                        </button>
                                        <i class="fa fa-check"></i> <?php echo $text_settings_save_success; ?>
                                    </div>
                                    <?php foreach ($settings as $data) { ?>
                                    <div class="form-group">
                                        <?php if ($data['type'] == 'text') { ?>
                                        <label class="col-sm-3 control-label"
                                               for="<?php echo $data['name']; ?>"><?php echo $data['label']; ?>:</label>
                                        <div class="col-sm-9">
                                            <input class="form-control" name="<?php echo $data['name']; ?>" type="text"
                                                   value="<?php echo $data['value']; ?>"
                                                   placeholder="<?php echo $data['label']; ?>">
                                            <div class="error-message"></div>
                                        </div>
                                        <?php } elseif( $data['type'] == 'select') { ?>
                                        <label class="col-sm-3 control-label"
                                               for="<?php echo $data['name']; ?>"><?php echo $data['label']; ?>:</label>
                                        <div class="col-sm-9">
                                            <select class="form-control" name="<?php echo $data['name']; ?>"
                                                    placeholder="<?php echo $data['label']; ?>">
                                                <option value="1"
                                                <?php if($data['value'] == '1'){ echo 'selected';} ?>
                                                ><?php echo 'Enable'; ?></option>
                                                <option value="0"
                                                <?php if($data['value'] == '0'){ echo 'selected';} ?>
                                                ><?php echo 'Disable'; ?></option>
                                            </select>
                                            <div class="error-message"></div>
                                        </div>
                                        <?php } else { ?>
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <div class="checkbox">
                                                <input name="<?php echo $data['name']; ?>" value="0" type="hidden">
                                                <label><input
                                                            name="<?php echo $data['name']; ?>" <?php echo $data['value'] == 1 ? 'checked' : ''; ?>
                                                    value="1" type="checkbox"> <?php echo $data['label']; ?></label>
                                            </div>
                                        </div>
                                        <?php } ?>
                                        <?php if (isset($data['help'])) { ?>
                                        <div class="col-sm-offset-3 col-sm-9 help-block">
                                            <?php echo $data['help']; ?>
                                        </div>
                                        <?php } ?>
                                    </div>
                                    <?php } ?>
                                    <div class="alert alert-info">
                                        <?php echo $label_confirmed_status; ?><br>
                                        <?php foreach($order_statuses as $item) { ?>
                                        <?= $item['order_status_id'] ?> - <?= $item['name'] ?><br>
                                        <?php } ?>
                                    </div>
                                    <div class="form-group">
                                        <div class="col-sm-offset-3 col-sm-9">
                                            <button type="submit" class="btn btn-primary"><i
                                                        class="fa fa-save"></i> <?php echo $button_update; ?></button>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    <div class="col-sm-12 col-md-12 col-lg-6">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h3 class="panel-title"><i class="fa fa-repeat"></i> <?php echo $text_cron; ?></h3>
                            </div>
                            <div class="list-group">
                                <?php foreach ($cron_urls as $name => $data) { ?>
                                <div class="list-group-item">
                                    <div class="input-group">
                                        <input type="text" class="form-control" value="<?php echo $data['url'] ?>">
                                        <div class="input-group-btn">
                                            <a href="<?php echo $data['url'] ?>" type="button " target="_blank"
                                               class="btn btn-primary"><i class="fa fa-play-circle"
                                                                          aria-hidden="true"></i> <?php echo $run_cron; ?>
                                            </a>
                                        </div>
                                    </div>
                                    <?php 
                                    if ($name == 'products'){
                                    echo $text_cron_description_products;
                                    }
                                    if ($name == 'fetchUpdatedProducts'){
                                    echo $text_cron_description_fetchUpdatedProducts;
                                    }
                                    else{
                                    echo $text_cron_description_orders;
                                    }
                                    ?>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<script type="text/javascript" language="javascript"
        src="https://cdn.datatables.net/1.10.13/js/jquery.dataTables.min.js"></script>
<link href="https://cdn.datatables.net/1.10.13/css/jquery.dataTables.min.css" type="text/css" rel="stylesheet"/>
<?php echo $footer; ?>